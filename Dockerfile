FROM sharelatex/sharelatex:5.3.1

LABEL maintainer="blq@laquadrature.net"

ENV DEBIAN_FRONTEND="noninteractive"
ENV PATH="${PATH}:/usr/local/texlive/2024/bin/x86_64-linux"

# Update system
RUN apt-get update && \
	apt-get dist-upgrade -y && \
	apt-get install -y tzdata && \
	apt-get clean && \
	rm -rf /var/lib/apt/lists

# Reinstall TexLive
RUN rm -rf /usr/local/texlive && \
	wget https://mirror.ctan.org/systems/texlive/tlnet/install-tl-unx.tar.gz && \
	tar -xzf install-tl-unx.tar.gz && \
	cd install-tl-* && \
	perl ./install-tl --no-interaction --no-doc-install --no-src-install --scheme=small && \
	cd .. && \
	rm -rf install-tl-unx.tar.gz install-tl-*

# Update packages
RUN tlmgr update --self && tlmgr update texlive-scripts && tlmgr update --all && tlmgr install scheme-full
